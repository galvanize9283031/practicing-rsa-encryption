# Practicing RSA Encryption and Decryption

## Lessons Learned from practicing RSA encryption

### Limitations of data size for RSA encryption

RSA encryption has a limitation on the size of the data it can encrypt based on
the key size. The error message "data too large for key size" indicates that the
data you are trying to encrypt is larger than the maximum size supported by the
RSA key size you have.

To fix this issue, you have a few options:

1. Use Hybrid Encryption: If you need to encrypt large amounts of data, you can
use hybrid encryption. Generate a symmetric key (like AES) to encrypt the data,
and then encrypt the symmetric key with RSA. This way, you only need to encrypt
a small amount of data with RSA.

1. Increase Key Size: You can generate a new RSA key pair with a larger key
size. Larger key sizes support encrypting larger amounts of data, but keep in
mind that larger key sizes also result in slower encryption and decryption
operations.

1. Split the Data: If you cannot change the key size and the data is too large,
you can split the data into smaller chunks and encrypt each chunk separately.
However, you'll need to ensure proper handling and sequencing of the chunks
during decryption.

On this code I used the first option, I used a symmetric key (AES) to encrypt
the data, and then encrypt the symmetric key with RSA.
