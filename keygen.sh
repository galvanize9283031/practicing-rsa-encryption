# Generate RSA private and public keys
if [ ! -f private_key.pem ]; then
    echo "Generating keys..."
    openssl rand --base64 32 > password.txt
    openssl genpkey -algorithm RSA -out private_key.pem -aes256 -pass file:password.txt
    openssl rsa -pubout -in private_key.pem -out public_key.pem -passin file:password.txt
fi

openssl rand -base64 2048 > message.txt

# Test the keys with signing and verification
openssl dgst -sha256 -sign private_key.pem -out signature.bin -passin file:password.txt message.txt
openssl dgst -sha256 -verify public_key.pem -signature signature.bin -passin file:password.txt message.txt

# Test the keys with encryption and decryption
# Generate AES symmetric key
openssl rand -out aes.key 32
# Encrypt data with AES
openssl enc -pbkdf2 -salt -in message.txt -base64 -out encrypted_msg.bin -pass file:aes.key
# Encrypt AES key with RSA public key
openssl pkeyutl -encrypt -pubin -inkey public_key.pem -in aes.key -out encrypted_key.bin
rm aes.key
# Decrypt AES key with RSA private key
openssl pkeyutl -decrypt -inkey private_key.pem -in encrypted_key.bin -out decrypted_key.bin -passin file:password.txt
# Decrypt data with AES
openssl enc -d -pbkdf2 -base64 -in encrypted_msg.bin -out decrypted_msg.txt -pass file:decrypted_key.bin
diff message.txt decrypted_msg.txt

# cleanup
rm -f *.bin *.txt *.pem
